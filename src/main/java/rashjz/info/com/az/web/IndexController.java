/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rashjz.info.com.az.web;

import java.io.Serializable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import rashjz.info.com.az.domain.AppUser;
import rashjz.info.com.az.domain.Users;
import rashjz.info.com.az.util.AuthoritiesConverter;
import rashjz.info.com.az.util.SecurityUtil;

/**
 *
 * @author Rashad Javadov
 */
@Controller
public class IndexController implements Serializable {

    private static final Logger logger = LoggerFactory.getLogger(IndexController.class.getName());

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String getIndexPage(Model model) {
        return "redirect:/login";
    }

    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public String getLoginPage(Model model) {
        return "login";
    }

    @RequestMapping(value = "/admin", method = RequestMethod.GET)
    public String getIndexAdmin(Model model) {
        return "admin";
    }

//    @ResponseBody
    @RequestMapping(value = "authUser", method = RequestMethod.POST)
    public String getIndexJson(@RequestBody Users user) {
        logger.info("*************  " + user.toString());
        //pass security manually get roles and datas from db and create appUser object 
        AppUser appUser=new AppUser(user, user.getFirstname(),  user.getLastname(), true, true, true, true, AuthoritiesConverter.getAuthorities(null));
        SecurityUtil.authenticateUser(appUser);
        return "admin";
    }

}
