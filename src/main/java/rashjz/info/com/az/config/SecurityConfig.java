package rashjz.info.com.az.config;

import rashjz.info.com.az.util.UserLoginSuccessHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;

/**
 *
 * @author Rashad Javadov
 */
@Configuration
@EnableWebSecurity
@PropertySource("classpath:application.properties")
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    Environment env;

    @Bean(name = "dataSource")
    public DriverManagerDataSource dataSource() {
        System.out.println(env.getProperty("database.driver"));
        DriverManagerDataSource driverManagerDataSource = new DriverManagerDataSource();
        driverManagerDataSource.setDriverClassName(env.getProperty("database.driver"));
        driverManagerDataSource.setUrl(env.getProperty("database.url"));
        driverManagerDataSource.setUsername(env.getProperty("database.username"));
        driverManagerDataSource.setPassword(env.getProperty("database.password"));
        return driverManagerDataSource;
    }

    @Autowired
    public void configAuthentication(AuthenticationManagerBuilder auth) throws Exception {

        auth.jdbcAuthentication().dataSource(dataSource())
                .usersByUsernameQuery(
                        "select username,password, enabled from users where username=?")
                .authoritiesByUsernameQuery(
                        "select username, role from user_roles where username=?");
    }
//    @Autowired
//    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
//        auth.inMemoryAuthentication().withUser("rashjz").password("12").roles("USER"); 
//    }

    @Bean
    public AuthenticationSuccessHandler successHandler() {
        return new UserLoginSuccessHandler();
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {

        http.authorizeRequests()
                //                   "/auth/**",
                //                                "/login",
                //                                "/signup/**",
                //                                "/user/register/**"
                .antMatchers("/admin", "/user").access("hasRole('ROLE_USER')")
                .antMatchers("authUser").access("PermitAll")
                .and()
                .formLogin().loginPage("/login").failureUrl("/login?error")
                .successHandler(successHandler())
                .usernameParameter("ssoId").passwordParameter("password")
                .and()
                .logout().logoutSuccessUrl("/login?logout")
                .and()
                .exceptionHandling().accessDeniedPage("/403")
                .and()
                .csrf();
    }
}
