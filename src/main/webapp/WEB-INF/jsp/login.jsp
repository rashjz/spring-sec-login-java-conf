<%-- 
    Document   : logintemplate
    Created on : Jul 4, 2016, 4:54:51 PM
    Author     : Rashad Javadov
--%>

<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%> 
<!DOCTYPE html>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Login Page</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
        <script type="text/javascript" src="//code.jquery.com/jquery-1.12.0.min.js"></script> 
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

    </head>



    <body>
        <div id="fb-root"></div>
        <script>

            function checkLoginState() {
                FB.getLoginStatus(function (response) {
                    statusChangeCallback(response);
                });
            }



            (function (d, s, id) {
                var js, fjs = d.getElementsByTagName(s)[0];
                if (d.getElementById(id))
                    return;
                js = d.createElement(s);
                js.id = id;
                js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.7&appId=888848437872798";
                fjs.parentNode.insertBefore(js, fjs);
            }(document, 'script', 'facebook-jssdk'));




            window.fbAsyncInit = function () {
                FB.init({
                    appId: '88xxxx378xxxxx8',
                    cookie: true, // enable cookies to allow the server to access 
                    // the session
                    xfbml: true, // parse social plugins on this page
                    version: 'v2.5' // use graph api version 2.5
                });

                FB.getLoginStatus(function (response) {
                    statusChangeCallback(response);
                });
            };

            function statusChangeCallback(response) {
                if (response.status === 'connected') {
//                        var accessToken = response.authResponse.accessToken;
                    FB.login(function (response) {
                        var token = response.authResponse.accessToken;
                        var uid = response.authResponse.userID;
                        if (response.authResponse) {
                            FB.api('/me', 'get', {access_token: token, fields: 'id,name,gender,email,first_name,last_name,picture'}, function (response) {
                                console.log(response);
//creating user json from response object
                                var user = {};
                                user["username"] = response.email;
                                user["firstname"] = response.first_name;
                                user["lastname"] = response.last_name;
                                user["password"] = response.id;
                                user["note"] = response.picture.data.url;
                                //ajax request to server from jsp
                                $.ajax({
                                    contentType: "application/json; charset=utf-8",
                                    dataType: "json",
                                    type: 'POST',
                                    url: "${pageContext.request.contextPath}/authUser?${_csrf.parameterName}=${_csrf.token}",
                                                                data: JSON.stringify(user),
                                                                success: function (data) {
                                                                    console.log("SUCCESS: ", data);

                                                                },
                                                                error: function (callback) {
                                                                    console.log(callback);
                                                                },
                                                                done: function (e) {
                                                                    console.log("DONE");
                                                                }
                                                            });


                                                        });

                                                    }
                                                },
                                                        {scope: 'public_profile'}
                                                );
                                            } else {
                                                console.log('connected to fb' + response.id);
                                            }

                                        }


        </script>




        <spring:url value="/register" var="registerUrl"/>
        <div class="container">
            <div id="loginbox" style="margin-top:15%; "  class="mainbox col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">                    
                <div class="panel panel-primary" >
                    <div class="panel-heading">
                        <div class="panel-title">Sign In</div>
                        <!--<div style="float:right; font-size: 80%; position: relative; top:-10px"><a href="#">Forgot password?</a></div>-->
                    </div>     

                    <div style="padding-top:30px;" class="panel-body" >

                        <div style="display:none" id="login-alert" class="alert alert-danger col-sm-12"></div>
                        <spring:url var="loginUrl" value="/login" />
                        <form id="loginform" action="${loginUrl}" method="post" class="form-horizontal" role="form">

                            <c:if test="${param.error != null}">
                                <div class="alert alert-danger">
                                    <p>Invalid username and password.</p>
                                </div>
                            </c:if>
                            <c:if test="${param.logout != null}">
                                <div class="alert alert-success">
                                    <p>You have been logged out successfully.</p>
                                </div>
                            </c:if>

                            <div style="margin-bottom: 25px" class="input-group">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                                <input id="username" type="text" class="form-control" name="ssoId" value="" placeholder="username or email">                                        
                            </div>

                            <div style="margin-bottom: 25px" class="input-group">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
                                <input id="password" type="password" class="form-control" name="password" placeholder="password">
                            </div>



                            <div class="input-group">
                                <div class="checkbox">
                                    <label>
                                        <input id="login-remember" type="checkbox" name="remember" value="1"> Remember me
                                    </label>
                                </div>
                            </div>

                            <input type="hidden" name="${_csrf.parameterName}" 	value="${_csrf.token}" />

                            <div style="margin-top:10px" class="form-group">
                                <!-- Button -->

                                <div class="col-sm-12 controls">
                                    <input id="btn-login" type="submit" value="Login" class="btn btn-primary">   
                                    <div class="fb-login-button" style="margin-left: 30px;" data-max-rows="1" data-size="large" data-show-faces="false"
                                         onlogin="checkLoginState();" data-auto-logout-link="false"></div>
                                </div>
                            </div>


                        </form>     



                    </div>                     
                </div>  
            </div>











        </div> 
    </div>


</body>
</html>