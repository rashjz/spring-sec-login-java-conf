<%-- 
    Document   : index
    Created on : Sep 19, 2016, 3:00:56 PM
    Author     : Mobby
--%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@page contentType="text/html" pageEncoding="windows-1251"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=windows-1251">
        <title>JSP Page user</title>
    </head>
    <body>
        <h1>User Page! <br/>

            <sec:authorize access="isAuthenticated()">
                <sec:authentication var="username" scope="request" property="principal.users.username"/> 
                <sec:authentication var="imgUrlProfile" scope="request" property="principal.users.note"/>

                <img style="height: 40px;border: 2px solid;border-radius: 25px;" 
                     onerror="http://www.brentfordfc.co.uk/images/common/bg_player_profile_default_big.png"  src="${imgUrlProfile}"/>
                 <br/>
                ${username} 
            </sec:authorize>
        </h1>
    </body>
</html>
